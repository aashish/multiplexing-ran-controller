#!/bin/bash
echo "Downloading Prometheus Node_Exporter..."
cd ~
sudo wget https://github.com/prometheus/node_exporter/releases/download/v1.0.1/node_exporter-1.0.1.linux-amd64.tar.gz
sudo tar -xzf node_exporter-1.0.1.linux-amd64.tar.gz
sudo rm -r node_exporter-1.0.1.linux-amd64.tar.gz
echo "Download and Extraction Complete."
echo "Starting Node Exporter"
cd ~/node_exporter-1.0.1.linux-amd64 && sudo ./node_exporter &
