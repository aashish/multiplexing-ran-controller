#!/bin/bash
echo "Downloading Grafana Server..."
cd ~
sudo wget https://dl.grafana.com/oss/release/grafana-7.0.5.linux-amd64.tar.gz
sudo tar -xzf grafana-7.0.5.linux-amd64.tar.gz
sudo rm -r grafana-7.0.5.linux-amd64.tar.gz
echo "Download and Extraction Complete."
echo "Starting Grafana..."
cd ~/grafana-7.0.5/bin/ && sudo ./grafana-server &

echo "Downloading Prometheus Node_Exporter..."
cd ~
sudo wget https://github.com/prometheus/node_exporter/releases/download/v1.0.1/node_exporter-1.0.1.linux-amd64.tar.gz
sudo tar -xzf node_exporter-1.0.1.linux-amd64.tar.gz
sudo rm -r node_exporter-1.0.1.linux-amd64.tar.gz
echo "Download and Extraction Complete."
echo "Starting Node Exporter"
cd ~/node_exporter-1.0.1.linux-amd64 && sudo ./node_exporter &

echo "Downloading Prometheus Server..."
cd ~
sudo -H pip3 install prometheus_client
sudo wget https://github.com/prometheus/prometheus/releases/download/v2.19.2/prometheus-2.19.2.linux-amd64.tar.gz
sudo tar -xzf prometheus-2.19.2.linux-amd64.tar.gz
sudo rm -r prometheus-2.19.2.linux-amd64.tar.gz
sudo mv /local/repository/etc/prometheus.yml ~/prometheus-2.19.2.linux-amd64
sudo mv /local/repository/etc/alert_rules.yml ~/prometheus-2.19.2.linux-amd64
echo "Download and Extraction Complete."
echo "Starting Node Exporter"
cd ~/prometheus-2.19.2.linux-amd64 && sudo ./prometheus &