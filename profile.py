#!/usr/bin/env python
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.urn as URN

tourDescription = """
# Multiplexing RAN Controller 
Use this profile to intantiate the Multiplexing RAN Controller architecture with the 5G-Empower software defined networking platform
within a controlled RF environment and end-to-end LTE network
(wired connections between UEs and eNBs). The UE(s) can be srsLTE-based or a Nexus 5.

If you elect to use a Nexus 5, these nodes will be deployed:
* Nexus 5 (`rue1`)
* Generic Compute Node w/ ADB image (`adbnode1`)
* Generic Compute Node w/ Empower image (`controller1`)
* Intel NUC5300/B210 w/ srsLTE eNB/EPC (`enb1`)

Additionally, if the second instance option was selected and you have elected to use a Nexus 5, these nodes will be deployed:
* Nexus 5 (`rue2`)
* Generic Compute Node w/ ADB image (`adbnode2`)
* Generic Compute Node w/ Empower image (`controller2`)
* Intel NUC5300/B210 w/ srsLTE eNB/EPC (`enb2`)

If instead you choose to use an srsLTE UE, these will be deployed:
* Intel NUC5300/B210 w/ srsLTE (`rue1`) or
* Intel NUC5300/B210 w/ srsLTE eNB/EPC (`enb1`)

Additionally, if the second instance option was selected and you have chosen to use an srsLTE UE, these nodes will be deployed:
* Intel NUC5300/B210 w/ srsLTE (`rue2`) or
* Intel NUC5300/B210 w/ srsLTE eNB/EPC (`enb2`)
"""
tourInstructions = """
#### NOTE: Ensure that the startup has finished for both the eNB(s) and the controller(s) via the powder ssh list view.

### Master Controller
If you have selected two instances, ignore this step for the second set of nodes.

Login to `controller1` and do:
```
cd /opt/empower-runtime/ && sudo python3 MasterController.py
```

To configure the Validation thresholds for monitoring application control messages, the permitted ip addresses, the validation policy, and the controller ip instances, please
update the `config.ini` file.
By default, the `Master Controller` is to be instantiated on the `controller1` node. 

### Admin Applications
#### Protocol
Admin applications directly interface with the Master Controller and are granted access to issue cross-instance and cross-slice commands.
To issue a command to the `Master Controller`, create a message (utf-8) in the format below and send it via TCP to {MasterController-IP}:9999: 
```
ADMIN\\n\\n\\n[Command] [Args]
```
On receiving a response from the `Master Controller`, the response will be prefixed with either `TEXT` or `OBJ` in utf-8 encoding.  If the response is prefixed with `OBJ`, then 
the response will correspond to a pickled python dictionary. To load the data, use `pickle.loads(pickled_data)`.
```
TEXT\\n\\n\\n[Message]
OBJ\\n\\n\\n[Pickled Data]
```

The list of supported commands are listed below:
* `test` - Sends a test command. A response of 'ok' indicates a successful interaction.
```
ADMIN\\n\\n\\ntest
```
* `get-all` - Requests all the project information for all active instances. 
```
ADMIN\\n\\n\\nget-all
```
* `create-project` - Creates a new project on an instance. 
```
ADMIN\\n\\n\\ncreate-project {Instance_ID}
```
* `create-slice` - Creates a new slice on a specific project. 
```
ADMIN\\n\\n\\ncreate-slice {Instance_ID} {Project_ID} {Slice_ID}
```
* `get-slices` - Requests all the slice information pertaining to a project. 
```
ADMIN\\n\\n\\nget-slices {Instance_ID} {Project_ID}
```
* `update-slice` - Updates slice properties. 
```
ADMIN\\n\\n\\nupdate-slice {Instance_ID} {Project_ID} {Slice ID} {OPTIONAL: RB} {OPTIONAL: UE_SCHEDULER}
```
* `kill` - Removes an entire project or a specific slice. 
```
ADMIN\\n\\n\\nkill {Instance_ID} {Project_ID} {OPTIONAL: Slice ID}
```
* `start-app` - Starts a slice application on a specific project. 
```
ADMIN\\n\\n\\nstart-app {Instance_ID} {Project_ID} {Application Type} {Application Args}
```
* `start-worker` - Starts a worker application on specific instance. 
```
ADMIN\\n\\n\\nstart-worker {Instance_ID} {Worker Type} {Worker Args}
```
* `kill-app` - Stops a slice application on a specific project. 
```
ADMIN\\n\\n\\nkill-app {Instance_ID} {Project_ID} {Application_ID}
```
* `kill-worker` - Stops a worker application on specific instance. 
```
ADMIN\\n\\n\\nkill-worker {Instance_ID} {Worker_ID}
```
* `get-apps` - Requests all the application information for a specific project. 
```
ADMIN\\n\\n\\nget-apps {Instance_ID} {Project_ID}
```
* `get-workers` - Requests all the worker application information for a specific instance. 
```
ADMIN\\n\\n\\nget-workers {Instance_ID}
```
* `get-measurements` - Requests all UE Measurements. 
```
ADMIN\\n\\n\\nget-measurements {OPTIONAL: Instance_ID} {OPTIONAL: IMSI}
```

Arguments:
* `Instance_ID` - Integer corresponding to the instances configured in the `config.ini` file
* `Project_ID` - Unique hash id. Can be retrieved from Empower UI or via the `get-all` command.
* `Slice_ID` - Integer id. Can be retrieved from Empower UI or via the `get-all` and `get-slices` commands.
* `Application_ID` - Unique hash id. Can be retrieved from Empower UI or via the `get-apps` command.
* `Worker_ID` - Unique hash id. Can be retrieved from Empower UI or via the `get-workers` command.
* `RB` - Integer indicating the number of resource block groups.
* `UE_SCHEDULER` - Integer indicating the scheduler type.
* `Application Type` - String indicating the application type.
* `Application Args` - Vary between applications. 
* `Worker Type` - String indicating the worker application type.
* `Worker Args` - Vary between worker applications. 
* `IMSI` - Integer indicating the IMSI of the specific UE.

Supported Application Types and Arguments: 
* `ue-measurements`:
    * `IMSI` - Integer indicating the IMSI of the specific UE.
    * `Meas_ID` - Integer indicating the desired measurement ID.

Supported Worker Types and Arguments: 
* `mac-prb-util`
    * `Every` - Integer indicating the delay in ms for utilization updates.

Supported UE Schedulers: 
* `Round Robin` represented by `0`

#### Sample Application
A sample admin application has been provided. The sample application can be run from both `controller1` and `controller2`.
Before executing commands on the application, ensure the 5G-Empower Controller(s) are running.
To run the sample application, do:
```
cd /opt/empower-runtime/ && sudo python3 Sample_Admin.py
```
The sample application emulates a shell. To execute commands on the `Master Controller`, simply type the commands into the shell window.
The response will then be displayed within the shell window. An example command and response is illustrated below.
```
admin@Controller:~$ start-app 0 02798480-3A9C-4ABD-8CCF-7F014AB18299 ue-measurements 998981234560301
Starting slice service...
started slice application
```

### 5G-Empower Controller
Login to `controller1` and do:
```
# start empower controller
cd /opt/empower-runtime/ && sudo ./empower-runtime.py
```

When creating a project on the 5G-Empower Controller, the default vbs address is `00:00:00:00:01:9B` and the default PLMNID is `99898`

For information about configuring the controller(s) visit:
https://github.com/5g-empower/5g-empower.github.io/wiki/Configuring-up-the-Controller

### Start EPC and eNB
After your experiment becomes ready, login to `enb1` via `ssh` and run the commands below in separate tabs:
```
# start srsepc
sudo srsepc /etc/srslte/epc.conf
# start srsenb
cd /opt/srsLTE-20.04/build/srsenb/src && sudo ./srsenb enb.conf
```
This will start the srsEPC and srseNB respectively. After you've associated
a UE with this eNB, you can use the third tab to run tests with `ping` or
`iperf`.
#### NOTE: If the radio is not detected, power cycle the eNB node.
### Nexus 5
If you've deployed a Nexus 5, you should see it sync with the eNB eventually and
obtain an IP address. Login to `adbnode1` in order to access `rue1` via `adb`:
```
# on `adbnode`
pnadb -a
adb shell
```
Once you have an `adb` shell to `rue1`, you can use `ping` to test the
connection, e.g.,
```
# in adb shell connected to rue1
# ping SGi IP
ping 172.16.0.1
```
If the Nexus 5 fails to sync with the eNB, try rebooting it via the `adb` shell.
After reboot, you'll have to repeat the `pnadb -a` and `adb shell` commands on
`adbnode1` to reestablish a connection to the Nexus 5.
### srsLTE UE
If you've deployed an srsLTE UE, login to `rue1` and do:
```
/local/repository/bin/start.sh
```
This will start a `tmux` session with two panes: one running srsue and the other
holding a spare terminal for running tests with `ping` and `iperf`.
If you are not familiar with `tmux`, it's a terminal multiplexer that
has some similarities to screen. Here's a [tmux cheat
sheet](https://tmuxcheatsheet.com), but `ctrl-b o` (move to other pane) and
`ctrl-b x` (kill pane), should get you pretty far. `ctrl-b d` will detach you
from the `tmux` session and leave it running in the background. You can reattach
with `tmux attach`. If you'd like to run `srsue` manually, do:
```
sudo srsue /etc/srslte/ue.conf
```
### The Second Instance
Follow the above instructions with the second set of nodes to set up a second instance of 5g-Empower.

### Prometheus Monitoring and Grafana
By default, the Prometheus Server, the Master Controller Metrics Server, and the Grafana Server are running on the `controller1` node.
The controller node exporters are running on their respective controller instances.
To view the logged network statistics visit:
* Prometheus Server: http://{controller1-node-ip}:9090
* Master Controller Metrics: http://{controller1-node-ip}:7999
* Controller(s) Node Exporter: http://{controller-node-ip}:9100
* Grafana Server: http://{controller1-node-ip}:3000
    * The default login is `admin` for both the username and the password.

#### NOTE: Grafana must be configured to pull data from the Prometheus Server running on `controller1` 
After linking Prometheus and Grafana, you must create your dashboards to view the timeseries information.
The recommended pre-built dashboards for visualizing node exporter data can be imported into Grafana via their ids: `405` and `1860`.

"""


# Experimental Images
# Controller image: urn:publicid:IDN+emulab.net+image+reu2020:Empower-Controller
# nextEPC eNb image: urn:publicid:IDN+emulab.net+image+reu2020:Empower-eNb-epc
# no next eNB image: urn:publicid:IDN+emulab.net+image+reu2020:srsLTE_Empower.enb1


class GLOBALS(object):
    NUC_HWTYPE = "nuc5300"
    COTS_UE_HWTYPE = "nexus5"
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    CONTROL_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU20-64-STD"  # Used Ubuntu20
    SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+PowderProfiles:U18LL-SRSLTE:1"  # Low-latency image
    ENB_IMG = "urn:publicid:IDN+emulab.net+image+reu2020:srsLTE_Empower.enb1"
    COTS_UE_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ANDROID444-STD")
    ADB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU14-64-PNTOOLS")


pc = portal.Context()
# Allow the user to use a Nexus 5 or srsUE
pc.defineParameter("ue_type", "UE Type", portal.ParameterType.STRING, "nexus5",
                   [("srsue", "srsLTE UE (B210)"), ("nexus5", "COTS UE (Nexus 5)")],
                   longDescription="Type of UE to deploy.")

# Allow the user to specify a specific UE
pc.defineParameter("FIXED_UE", "Bind to a specific UE",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a POWDER controlled RF UE node to allocate (e.g., 'ue1').  "
                                   "Leave blank to let the mapping algorithm choose.")
# Allow the user to specify a specific eNb
pc.defineParameter("FIXED_ENB", "Bind to a specific eNodeB",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a POWDER controlled RF eNodeB device to allocate (e.g., 'nuc1')."
                                   "  Leave blank to let the mapping algorithm choose.  "
                                   "If you bind both UE and eNodeB devices, "
                                   "mapping will fail unless there is path between them via the attenuator matrix.")
                                   
# Allow the user to specify whether two instances should be instantiated.
pc.defineParameter("two_inst", "Two Instances", portal.ParameterType.BOOLEAN, False, advanced=True,
                   longDescription="Allows two instances of empower-setups to be instantiated.")
                                   
# Allow the user to use a second Nexus 5 or srsUE
pc.defineParameter("ue_type2", "UE Type2", portal.ParameterType.STRING, "nexus5",
                   [("srsue", "srsLTE UE (B210)"), ("nexus5", "COTS UE (Nexus 5)")], advanced=True,
                   longDescription="NOTE: Will only be used if second instance is checked in advanced.")

# #Allow the user to specify a second UE
pc.defineParameter("FIXED_UE2", "Bind to a second UE",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="NOTE: Will only be used if second instance is checked in advanced.")
# #Allow the user to specify a second eNb
pc.defineParameter("FIXED_ENB2", "Bind to a second eNodeB",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="NOTE: Will only be used if second instance is checked in advanced.")

params = pc.bindParameters()

# Check for exceptions and warnings
pc.verifyParameters()

# Create model of the RSpec
request = pc.makeRequestRSpec()

# Add a NUC eNB node
enb1 = request.RawPC("enb1")
if params.FIXED_ENB:
    enb1.component_id = params.FIXED_ENB
enb1.hardware_type = GLOBALS.NUC_HWTYPE
enb1.disk_image = GLOBALS.ENB_IMG
enb1.Desire("rf-controlled", 1)
enb1_rue1_rf = enb1.addInterface("rue1_rf")

# Support for a single instance
if params.two_inst:
    enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files1.sh"))
else:
    enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))

# For second eNB
if params.two_inst:
    enb2 = request.RawPC("enb2")
    if params.FIXED_ENB2:
        enb2.component_id = params.FIXED_ENB2

    enb2.hardware_type = GLOBALS.NUC_HWTYPE
    enb2.disk_image = GLOBALS.ENB_IMG
    enb2.Desire("rf-controlled", 1)
    enb2_rue2_rf = enb2.addInterface("rue2_rf")
    enb2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files2.sh"))
    enb2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
    enb2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))

# Add a Controller Node
controller = request.RawPC("controller1")
controller.disk_image = GLOBALS.CONTROL_IMG
controller.addService(rspec.Execute(shell="bash", command="/local/repository/bin/Controller-Script1.sh"))
# Copy single instance master config file 
if not params.two_inst:
    controller.addService(rspec.Execute(shell="bash", command="/local/repository/bin/single_instance_config.sh"))
controller.addService(rspec.Execute(shell="bash", command="/local/repository/bin/get_monitoring_tools.sh"))



# Specify the component id and the IPv4 address for the Controller Node
iface1 = controller.addInterface("if1")
iface1.component_id = "eth1"
iface1.addAddress(rspec.IPv4Address("192.168.1.1", "255.255.255.0"))

# For second Controller 
if params.two_inst:
    controller2 = request.RawPC("controller2")
    controller2.disk_image = GLOBALS.CONTROL_IMG
    controller2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/Controller-Script2.sh"))
    controller2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/get_node_exporter.sh"))
    
    # Specify the component id and the IPv4 address for the Controller Node
    iface2 = controller2.addInterface("if2")
    iface2.component_id = "eth2"
    iface2.addAddress(rspec.IPv4Address("192.168.1.2", "255.255.255.0"))

# Add an EPC Node
# epc = request.RawPC("epc")
# epc.disk_image = GLOBALS.SRSLTE_IMG
# epc.addService(rspec.Execute(shell="bash", command="/local/repository/bin/install_nextEPC.sh"))

# Add a UE node
if params.ue_type == "nexus5":
    adbnode = request.RawPC("adbnode")
    adbnode.disk_image = GLOBALS.ADB_IMG
    rue1 = request.UE("rue1")
    if params.FIXED_UE:  # Use a specific UE
        rue1.component_id = params.FIXED_UE
    rue1.hardware_type = GLOBALS.COTS_UE_HWTYPE
    rue1.disk_image = GLOBALS.COTS_UE_IMG
    rue1.adb_target = "adbnode"
    rue1.Desire("rf-controlled", 1)
    rue1_enb1_rf = rue1.addInterface("enb1_rf")
elif params.ue_type == "srsue":
    rue1 = request.RawPC("rue1")
    if params.FIXED_UE:  # Use a specific UE
        rue1.component_id = params.FIXED_UE
    rue1.hardware_type = GLOBALS.NUC_HWTYPE
    rue1.disk_image = GLOBALS.SRSLTE_IMG
    rue1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    rue1.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
    rue1.Desire("rf-controlled", 1)
    rue1_enb1_rf = rue1.addInterface("enb1_rf")

# For a second UE node
if params.two_inst:
    if params.ue_type2 == "nexus5":
        adbnode2 = request.RawPC("adbnode2")
        adbnode2.disk_image = GLOBALS.ADB_IMG
        rue2 = request.UE("rue2")
        if params.FIXED_UE2:  # Use a specific UE
            rue2.component_id = params.FIXED_UE2
        rue2.hardware_type = GLOBALS.COTS_UE_HWTYPE
        rue2.disk_image = GLOBALS.COTS_UE_IMG
        rue2.adb_target = "adbnode2"
        rue2.Desire("rf-controlled", 1)
        rue2_enb2_rf = rue2.addInterface("enb2_rf")
    elif params.ue_type2 == "srsue":
        rue2 = request.RawPC("rue2")
        if params.FIXED_UE2:  # Use a specific UE
            rue2.component_id = params.FIXED_UE2
        rue2.hardware_type = GLOBALS.NUC_HWTYPE
        rue2.disk_image = GLOBALS.SRSLTE_IMG
        rue2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
        rue2.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
        rue2.Desire("rf-controlled", 1)
        rue2_enb2_rf = rue2.addInterface("enb2_rf")


# Create the RF link between the UE and eNodeB
rflink = request.RFLink("rflink")
rflink.addInterface(enb1_rue1_rf)
rflink.addInterface(rue1_enb1_rf)

# For second UE and eNodeB
if params.two_inst:
    rflink2 = request.RFLink("rflink2")
    rflink2.addInterface(enb2_rue2_rf)
    rflink2.addInterface(rue2_enb2_rf)

# Create the LAN and attach the devices
epclink = request.Link("s1-lan")
# epclink.addNode(epc)
epclink.addNode(enb1)
epclink.addNode(controller)

# For second LAN
if params.two_inst:
    epclink2 = request.Link("s2-lan")
    # epclink.addNode(epc)
    epclink2.addNode(enb2)
    epclink2.addNode(controller2)
    
    # Enable Connectivity between controller1 node and controller2 node
    link = request.Link(members = [controller, controller2])

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
